
package org.primefaces.showcase.domain;

public class Car {
	
	private String id,brand,color;
	private int year;
	
	public Car(String id, String brand, int year, String color) {
		this.id = id;
		this.brand = brand;
		this.color = color;
		this.year = year;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}

}
